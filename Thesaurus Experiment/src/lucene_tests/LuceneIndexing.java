package lucene_tests;

/*
 * Source: Lucene Version 3.0 Tutorial, Bob Carpenter et al., 2012
 * */

import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LimitTokenCountAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import com.aliasi.util.Files;

public class LuceneIndexing 
{
	public static void main(String[] args) throws IOException 
	{
		File docDir = new File("docDir");
		File indexDir = new File("indexDir");
		
		Directory fsDir = FSDirectory.open(indexDir);
		Analyzer stdAnalyzer = new StandardAnalyzer(Version.LUCENE_31);
		Analyzer ltcAnalyzer = new LimitTokenCountAnalyzer(stdAnalyzer, Integer.MAX_VALUE);
		IndexWriterConfig iwConf = new IndexWriterConfig(Version.LUCENE_31, ltcAnalyzer);
		iwConf.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
		IndexWriter indexWriter = new IndexWriter(fsDir, iwConf);
		
		for (File f : docDir.listFiles()) {
			String fileName = f.getName();
			String text = Files.readFromFile(f, "ASCII");
			Document d = new Document();
			d.add(new Field("file", fileName, Store.YES, Index.NOT_ANALYZED));
			d.add(new Field("text", text, Store.YES, Index.ANALYZED));
			indexWriter.addDocument(d);
		}
		
		int numDocs = indexWriter.numDocs();
		//indexWriter.forceMerge(1);
		System.out.println("numDocs: " + numDocs);
		indexWriter.commit();		
		indexWriter.close();
	}
}