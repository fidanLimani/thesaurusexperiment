package lucene_tests;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LimitTokenCountAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;

/*
 * Source: Lucene Version 3.0 Tutorial, Bob Carpenter et al., 2012
 * */

public class LuceneDelete {

	public static void main(String[] args) throws CorruptIndexException, LockObtainFailedException, IOException 
	{
		File indexDir = new File("indexDir");
		Directory fsDir = FSDirectory.open(indexDir);
		Analyzer stdAnalyzer = new StandardAnalyzer(Version.LUCENE_31);
		Analyzer ltcAnalyzer = new LimitTokenCountAnalyzer(stdAnalyzer, Integer.MAX_VALUE);
		
		IndexWriterConfig iwConf = new IndexWriterConfig(Version.LUCENE_31, ltcAnalyzer);
		iwConf.setOpenMode(IndexWriterConfig.OpenMode.APPEND);
		IndexWriter indexWriter = new IndexWriter(fsDir, iwConf);
		
		// Let's delete a document from the index
		int numDocsBefore = indexWriter.numDocs();
		String fieldName = "file";
		Term term = new Term(fieldName, "TheStory.txt");
		indexWriter.deleteDocuments(term);
		
		boolean hasDeletedDocs = indexWriter.hasDeletions();
		int numDocsAfterDeleteBeforeCommit = indexWriter.numDocs();
		indexWriter.commit();
				
		int numDocsAfter = indexWriter.numDocs();
		indexWriter.close();
		
		System.out.println("# of documents before deletion: " + numDocsBefore);
		System.out.println("Deleted documents? (True/False): " + hasDeletedDocs);
		System.out.println("Num docs after delete before commit: " + numDocsAfterDeleteBeforeCommit);
		System.out.println("Num docs after commit: " + numDocsAfter);
		
		System.out.println("IndexDir # of files: " + indexWriter.maxDoc() + "\n");
		
		System.out.println("The application completed successfully.");
	}
}