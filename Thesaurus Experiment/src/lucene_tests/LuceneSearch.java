package lucene_tests;

/*
 * Source: Lucene Version 3.0 Tutorial, Bob Carpenter et al., 2012
 * */

import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

public class LuceneSearch 
{
	public static void main(String[] args) throws 
							CorruptIndexException, IOException, org.apache.lucene.queryParser.ParseException 
	{
		File indexDir = new File("indexDir");
		String query = "tide";
		int maxHits = 15;
		
		// Create a Lucene directory, index reader, index searcher and query parser.
		Directory fsDir = FSDirectory.open(indexDir);
		IndexReader reader = IndexReader.open(fsDir);
		IndexSearcher searcher = new IndexSearcher(reader);
		String dField = "text";
		Analyzer stdAn = new StandardAnalyzer(Version.LUCENE_31);
		QueryParser parser = new QueryParser(Version.LUCENE_31, dField, stdAn);
		
		// Use the query parser to parse the query, then search the index and report the results.
		Query q = parser.parse(query);
		TopDocs hits = searcher.search(q, maxHits);
		ScoreDoc[] scoreDocs = hits.scoreDocs;
		
		for (int n = 0; n < scoreDocs.length; ++n) {
			ScoreDoc sd = scoreDocs[n];
			float score = sd.score;
			int docId = sd.doc;
			Document d = searcher.doc(docId);
			String fileName = d.get("file");
		}
		
		System.out.println("Search string: <<" + query + ">>. The application completed.");
	}
}