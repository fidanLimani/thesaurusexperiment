package lucene_tests;

/*
 * Adapted from "Lucene in Action", Chapter 1.
 * */

import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

public class Indexer 
{
	private IndexWriterConfig writerConfig;
	private Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_31);
	private IndexWriter indexWriter;
	Directory fsDir;
	
	public Indexer(String indexDir) throws IOException {
		fsDir = FSDirectory.open(new File(indexDir));		
		writerConfig = new IndexWriterConfig(Version.LUCENE_31, analyzer);
		indexWriter = new IndexWriter(fsDir, writerConfig);
	}
	
	public int index(String dataDir, FileFilter filter) throws Exception {
		File[] files = new File(dataDir).listFiles();
		
		for (File f: files) {
			if (!f.isDirectory() && !f.isHidden() && f.exists() && 
					f.canRead() && (filter == null || filter.accept(f))) {
				indexFile(f);
			}
		}
		
		return indexWriter.numDocs();
	}
	
	protected Document getDocument(File f) throws Exception {
		Document doc = new Document();
		doc.add(new Field("contents", new FileReader(f)));
		doc.add(new Field("filename", f.getName(), Field.Store.YES, Field.Index.NOT_ANALYZED));
		doc.add(new Field("fullpath", f.getCanonicalPath(), Field.Store.YES, Field.Index.NOT_ANALYZED));
		return doc;
	}
	
	private void indexFile(File f) throws Exception {
		System.out.println("Indexing " + f.getCanonicalPath());
		Document doc = getDocument(f);
		indexWriter.addDocument(doc);
	}
	
	private static class TextFilesFilter implements FileFilter {
		public boolean accept(File path) {
			return path.getName().toLowerCase().endsWith(".txt");
		}
	}
	
	public static void main(String[] args) throws Exception 
	{
		String docDir = "docDir";
		String indexDir = "indexDir";
		
		long start = System.currentTimeMillis();
		Indexer indexer = new Indexer(indexDir);
		
		int numIndexed;
		
		numIndexed = indexer.index(docDir, new TextFilesFilter());
		indexer.indexWriter.close();
		long end = System.currentTimeMillis();
		
		System.out.println("Indexing " + numIndexed + " files took " + (end - start) + " milliseconds");
	}
}