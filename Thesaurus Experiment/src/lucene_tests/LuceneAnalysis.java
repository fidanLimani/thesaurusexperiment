package lucene_tests;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;

public class LuceneAnalysis 
{
	public static void main(String[] args) throws IOException 
	{			
		// Apply an analyzer to a field name and text input
		String fieldName = "foo", text = "Mr. Sutton-Smith will pay $1.20 for the book.";
		
		StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_31);
		Reader textReader = new StringReader(text);
		TokenStream tokenStream = analyzer.tokenStream(fieldName, textReader);		
		CharTermAttribute terms = tokenStream.addAttribute(CharTermAttribute.class);		
		OffsetAttribute offsets = tokenStream.addAttribute(OffsetAttribute.class);
		
		PositionIncrementAttribute positions = tokenStream.addAttribute(PositionIncrementAttribute.class);
		
		// INCR (START, END) TERM
		System.out.println("INCR\t(START, END) \tTERM");
		while (tokenStream.incrementToken()) {
			int increment = positions.getPositionIncrement();
			int start = offsets.startOffset();
			int end = offsets.endOffset();
			String term = terms.toString();
			System.out.println(increment + "\t" + start + "\t" + end + "\t" + term );
		}
	}
}