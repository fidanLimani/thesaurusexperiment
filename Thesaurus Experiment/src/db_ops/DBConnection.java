package db_ops;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	String url = "jdbc:mysql://localhost:3306/";
	String dbName = "blogdb";
	String driver = "com.mysql.jdbc.Driver";
	String userName = "cst";
	String password = "pass";

	// SQL related:
	private Connection connection = null;	

	public Connection getConnection() {
		try {
			// Establish connection to the DBMS:
			connection = DriverManager.getConnection(url + "" + dbName, userName, password);
			// System.out.println("Successfully connected!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}
	
	public static void main(String[] args) {
		DBConnection dbc = new DBConnection();
		dbc.getConnection();
	}
}