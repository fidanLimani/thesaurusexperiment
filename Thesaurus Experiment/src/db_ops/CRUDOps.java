package db_ops;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import blog_crawling.Article;
import blog_crawling.Comment;

/*
 * Resources:
 * 
 * 1. http://stackoverflow.com/questions/2391851/how-to-insert-data-using-multiple-tables
 * 2. http://stackoverflow.com/questions/19714308/mysql-how-to-insert-into-table-that-has-many-to-many-relationship
 * 
 * Process description:
 * a. Insert row in the "parent" entity that creates an auto-increment id.
 * 	  Store it in a variable for later use.
 * b. Insert row in the "child" table where the value from LAST_INSERT_ID()
 *    function is put as a foreign key (referencing the "parent" entity's 
 *    primary key).
 * */

public class CRUDOps 
{
	private Connection conn;
	private DBConnection dbConn;
	
	// The last INSERT ID of the "parent" table 
	private int insertID;

	public CRUDOps() {
		this.conn = null;
		dbConn = new DBConnection();
	}
	
	// Read data from the DB
	public void performCreate(Article a){
		String blogPostQ = "Insert into blog_post(title, content, datePublished, author, postURL) "
				+ "VALUES (?, ?, ?, ?, ?) ";
		
		try {
			conn = dbConn.getConnection();
			
			// 0. Use a transaction begin: if any INSERT fails, rollback! We need to make sure that
			// all the other tables, not only the "parent" one, are completed with the Article object data
			conn.setAutoCommit(false);			
			
			// 1. Create the INSERT prepared statement for the <blog_post> table
			System.err.println(a.getURL());
			System.err.println("-----------------");
			
			PreparedStatement pStmt = conn.prepareStatement(blogPostQ);
			pStmt.setString(1, a.getTitle());
			pStmt.setString(2, a.getContent());			
			//pStmt.setDate(3, (Date) a.getPubDate());
			pStmt.setDate(3, (Date) a.getDate());
			pStmt.setString(4, a.getAuthor());
			pStmt.setString(5, a.getURL());
			
			// Execute the INSERT prepared statement			
			 pStmt.execute();
		     
		    
		     // Retrieve and store the ID of the blog post, to be used during the other 1:M INSERT operations.
		     PreparedStatement getLastPostID = conn.prepareStatement("SELECT LAST_INSERT_ID()");
		     ResultSet rs = getLastPostID.executeQuery();		    
		     if (rs.next()){
		    	 insertID = (int) rs.getLong("last_insert_id()");
		    	 // System.out.println("The ID of the blog_post insert is: " + insertID);
		     }
		     		     
		    // 2. Create the INSERT prepared statement for <social_media> table
		    String socialMediaQ = "INSERT INTO social_media(fbCount, twitterCount, blog_post_postID) VALUES (?, ?, ?)";
		    PreparedStatement pStmt2 = conn.prepareStatement(socialMediaQ);
		    pStmt2.setLong(1, a.getFbShares());
		    pStmt2.setLong(2, a.getTweets());
		    pStmt2.setInt(3, insertID);
		    // Execute the INSERT prepared statement for <social_media> table		    		    
		    pStmt2.execute();
		    		     
		     // 3. Create the INSERT prepared statement for <keywords> table
		    /*
		     * This INSERT statment is going to be used in line with MAUI tool; we need to
		     * test its performance withough and with a training data set. After a successful
		     * training evaluation, we will include these keywords in the <keywords> table to
		     * measure the blog post tags precision
		     */
		    String keywordsQ = "INSERT INTO keywords(keyword, blog_post_postID) VALUES (?, ?)";
		    PreparedStatement pStmt3 = conn.prepareStatement(keywordsQ);
		    
		    // At this point we need a method that automatically sets the prepared statements,
		    // depending on the number of keywords generated per Article object. <Article.comments> field.
		    
		    ArrayList<String> keywords = a.getKeywords();
		    for(String str : keywords){
		    	pStmt3.setString(1, str);
		    	pStmt3.setInt(2, insertID);
		    	// Execute the INSERT prepared statement for comments
			    pStmt3.execute();
		    }		  
		    
		    // 4. Create an INSERT prepared statement for the <tags> table.	
		    // As with the <comments> table, we need a method that will loop through all the tags
		    // associated with a blog post. Method input: <Article.tags> field.
		    String tagsQ = "INSERT INTO tags(tagName, blog_post_postID) VALUES (?, ?)";
		    PreparedStatement pStmt4 = conn.prepareStatement(tagsQ);
		    
		    ArrayList<String> tags = a.getTags();
		    for(String str: tags){
		    	pStmt4.setString(1, str);
		    	pStmt4.setInt(2, insertID);
		    	// Execute the INSERT prepared statement for comments
			    pStmt4.execute();
		    }
		   
		    // 5. Create an INSERT prepared statement for the <comments> table.
		    // As with the <comments> table, we need a method that will loop through all the tags
		    // associated with a blog post. Method input: <Article.comments> field.
		    String commentsQ = "INSERT INTO comments(comment, author, dateStamp, blog_post_postID) VALUES (?, ?, ?, ?)";
		    PreparedStatement pStmt5 = conn.prepareStatement(commentsQ);
		    
		    ArrayList<Comment> comments = a.getComments();
		    for(Comment c : comments){
		    	pStmt5.setString(1, c.getCommentContent());
		    	pStmt5.setString(2, c.getUserName());
		    	pStmt5.setDate(3, (Date) c.getDate());
		    	pStmt5.setInt(4, insertID);
		    	// Execute the INSERT prepared statement for comments
			    pStmt5.execute();			
		    }
		    
			// End the transaction block
		    conn.commit();
		} catch (SQLException e) {
			System.err.println("An exception while inserting!");
			System.err.println(e.getMessage());
			e.printStackTrace();
		}		
	}

	public static void main(String[] args) {	
		CRUDOps crud = new CRUDOps();		
		
		// crud.performCreate();
		
		System.out.println("<Application completed.>");
	}
}
