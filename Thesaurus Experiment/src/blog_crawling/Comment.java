package blog_crawling;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.lang3.StringEscapeUtils;

/*
 * This class encapsulates the properties and behavior of blog/wiki article
 * comments. 
 * */

public class Comment 
{
	// timeStamp; commenterName; commentContent
	private Date dateStamp;
	private java.sql.Date date;
	private String userName;
	private String commentContent;
	
	public Comment(Date dateStamp, String userName, String commentContent){
		this.dateStamp = dateStamp;
		this.userName = userName;
		this.commentContent = commentContent;
	}
		
	// Method for parsing String-valued date and time
	// Since we parse the date in the ListResources class, we could remove this method from
	// this class
	 static Date parseDate(String value) throws ParseException 
	 {
		String tempDate = value.replace(" ET ", " ");
		DateFormat df = new SimpleDateFormat("h:mm a MMM d, y");
		Date date = df.parse(tempDate);
		// System.out.println(date);
	 	return date; 	
	 }	
	
	public Date getDateStamp() {
		return dateStamp;
	}

	public void setDateStamp(Date dateStamp) {
		this.dateStamp = dateStamp;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCommentContent() {
		return commentContent;
	}

	public void setCommentContent(String commentContent) {
		this.commentContent = commentContent;
	}
	
	public String toString(){
		return "Date stamp: " + getDateStamp() + 
				"\nUser: " + getUserName() +
				"\nComment: " + getCommentContent();
	}
	
	// Util method: convert a java.util.Date to java.sql.Date type
	private java.sql.Date toSQLDate(java.util.Date date){
		return new java.sql.Date(date.getTime());
	}
	
	public java.sql.Date getDate(){
		return toSQLDate(this.dateStamp);
	}
	
	// Testing the app.
	public static void main(String[] args) throws ParseException
	{
		String tempDate = "3:05 pm ET Jul 15, 2015";					
		String user = "Frederic Court";
		String content = StringEscapeUtils.escapeJava("Great post Ed");		
		
		Comment c = new Comment(Comment.parseDate(tempDate), user, content);
		
		System.out.println(c.toString());
						
	}
}
