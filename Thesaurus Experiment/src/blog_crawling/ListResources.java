package blog_crawling;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/*
 * We are fetching blog articles from the WSJ Business Blogs, matching the following CSS rules: 
 * .wsjblog and .headlineSummary
 * */

public class ListResources 
{
	private int numArticles; // The total number of articles fetched in an app. run
	
	// This list stores all the (URLs of) articles for a given business topic
	private ArrayList<String> articleList;
    
    // The WSJ Blogs Categories by Business Topic
    String[] res = {"http://blogs.wsj.com/accelerators/",
        "http://blogs.wsj.com/bankruptcy/",
        "http://blogs.wsj.com/developments/",
        "http://blogs.wsj.com/digits/",
        "http://blogs.wsj.com/law/",
        "http://blogs.wsj.com/numbersguy/",
        "http://blogs.wsj.com/privateequity/",        
        "http://blogs.wsj.com/tech-europe/",
        "http://blogs.wsj.com/venturecapital/"};
    
    public ListResources() {
    	numArticles = 0;
    	articleList = new ArrayList<String>();
    }
    
    /* The following method lists all the articles of a category page
     * An example URL for a category: http://blogs.wsj.com/accelerators/page/4/
     * */     
    public void listArticles(String url, int pageNumber) throws IOException 
    {     	
    	url += "page/" + pageNumber;
    	// 20 s time out for Jsoup URL connection
    	Document doc = Jsoup.connect(url).timeout(20*1000).get();
    	
    	/*
    	 * Here we can invoke the getTags() method from class List Resources
    	 * */
    	
    	// Capture the article title for the given page (Page 1, for example)
    	// CSS rule: .post-snippet .post-title
    	
    	Elements articleLinks = doc.select(".post-snippet .post-title");
    	
    	for(Element e : articleLinks) {
    		String articleLink = e.text() + ": " + e.select("a").attr("href");
    		System.out.println(articleLink);
    		articleList.add(articleLink);
    		numArticles += articleLinks.size();
    	}
    	
    	System.out.println("\n# of articles: " + numArticles);    	
    }
    
    // Retrieve article elements
    public void articleElements(String url) throws IOException, ParseException {
       	// This time we need to connect to category URL, not blog URI
    	Document doc = Jsoup.connect("http://blogs.wsj.com/accelerators/page/1/").timeout(20*1000).get();
    	
    	// Invoke the method that extracts articles and their related tags
    	/* ArrayList<String> tagsPerArticle = new ArrayList();
    	tagsPerArticle = getTags(doc); */
    	    	
    	// All blog titles for this category page (Example, all articles on page 3)
    	Elements e = doc.select(".post-snippet .post-title");    	
    	// System.out.println("# of titles: " + e.size());  	
    	
    	// All blog article tags for this category page (Example, all article tags on page 3)
    	Elements tags = doc.select("ul.post-tags");    	
    	//System.out.println("<Tags #>: " + tags.size());
    	
    	// Store the set of tags of each article
    	ArrayList<String> myList;
    	//ArticleTag at;
    	ArrayList<ArticleTag> alTag = new ArrayList<ArticleTag>();
    	
    	// Assign article URL to tag sets for a category page:    	
    	Elements tagElmts = null;
    	for(int i=0; i<e.size(); i++) {
    		String articleTitle = e.get(i).text();
    		//System.out.println("Article: " + articleTitle);
    		
    		// Get an Element to hold objects from the <tags> of Elements
    		Element el = tags.get(i);
    		tagElmts = el.getElementsByTag("li");
    		//System.out.println("Tag list size: " + tagElmts.size());
    		myList = new ArrayList<String>();
    		for(Element myEl : tagElmts){
    			//System.out.println(myEl.text());   
    			myList.add(myEl.text());
    		}
    		
    		// Creat an ArticleTag object, and store article URL and tag list    		
    		alTag.add(new ArticleTag(articleTitle, myList));	
    	}
    	
    	System.out.println("TEST....");
    	System.out.println("# of ArticleTag objects in the array list <alTag>: " + alTag.size());
    	
    	System.out.println("Displaying article titles from the array list <alTag>...");
    	for(int i=0; i<alTag.size(); i++) {
    		System.out.println(alTag.get(i).getUrl());
    	}
    	
    	/*
    	Elements tagElmts = null;
    	for(Element el : tags) {     		    		
    		tagElmts = el.getElementsByTag("li");
    		System.out.println("Tag list size: " + tagElmts.size());    		
    		for(Element myEl : tagElmts){
    			System.out.println(myEl.text());   
    			myList.add(myEl.text());
    		}
    		System.out.println("----------------");
    	} */
    	
    	// Capture article author: Not possible through article scraping, as this
    	// information is embedded in text, or just displayed as a tag.
    	
    	//String author = doc.select(".blog-sp .post-content a").first().text();
    	//System.out.println("\n<Author> \t" + author);
    	
    	/*
    	// Capture article tags (as originally posted by the blog author)    	
    	String tags = doc.select(".blog-sp .postcats.post-tags-b").text();
    	String[] tagList = tags.split(" ");
    	System.out.println("<Tags #1> " + tags);
    	*/
    	
    	/* Note that is using this approach, we will have to fetch this data
    	* immediately after fetching the blog page category, for example:
    	* http://blogs.wsj.com/accelerators/page/1/.
    	*/
    	
    	/*
    	for(String tag : tagList){
    		System.out.println(tag + "\t");
    	} */
    	
    	// Capture article content
    	/*
    	String content = "";
    	Elements articleContent = doc.select(".blog-sp .post-content p");
    	
    	for(Element e : articleContent){
    		content += e.text();    		
    	}
    	
    	// Remove the additional "follow" links at the end
    	content = content.replaceAll("Follow @.*", ""); */   	    	 
    	
    	// System.out.println("\n<Content> \t" + content);
    	
    	// Capture date and time: sth similar is done in Comment class, too.
    	/*
    	String dateTime = doc.select(".post-header .post-time").text();    	    	
    	Date date = parseDate(dateTime);
    	System.out.println("\n<Date> " + date); */    	
   	
    	// Collect social media information; for SWJ this includes Facebook, Tweeter, and LinkedIn;
    	/*
        JsonReader json = new JsonReader();
        int fbShares = json.fbRecommend(url);
        System.out.println("\n<Facebook shares> " + fbShares); */
        
        // Tweets
        /*
        TwitterStats ts = new TwitterStats();
        int twitterCount = ts.twitterCount(url);
        System.out.println("\n<Twitter count> " + twitterCount); */
        
        // Get the article comments
        // Add the "tab/comments/" string to the article base URL
        /*
        String commentURL = url + "tab/comments";
        Document doc2 = Jsoup.connect(commentURL).get();     
        */
        
        // No of article comments
        /*
        Elements allComments = doc2.select(".commentlist .commententry");
        int commentsNo = allComments.size();
                        
        if(commentsNo == 0){
            System.out.println("\n<Comments> There are no comments for this post.");
         } else {
             System.out.println("\n<Comments> " + commentsNo);

             // The author of the comment & time stamp, and the content
             // Elements allComments = doc2.select("ol, ul.commentlist");             
             for (Element e : allComments) {
                 System.out.println("\nComment time stamp: " + e.select("li.poststamp").text());
                 System.out.println("User name: " + e.select("li.posterName cite").text());
                 System.out.println("Comment: " + e.select("div.commentContent p").text());
             }
         }
        */
 }
    
    // A method to get the tags/article from e blog category page:
    // Get tags/article from page 2 from "Accelerators" category
    public ArrayList<ArticleTag> getTags(String categoryURL) throws IOException 
    {
    	// Example: http://blogs.wsj.com/accelerators/page/1/ could be passed in
    	Document doc = Jsoup.connect(categoryURL).timeout(20*1000).get();

    	// All blog titles for this category page (Example, all articles on page 3)
    	// (Element)e.select("a").attr("href"); -> Article URL
    	Elements e = doc.select(".post-snippet .post-title").select("a");    	
    	// System.out.println("# of titles: " + e.size());  	
    	
    	// All blog article tags for this category page (Example, all article tags on page 3)
    	Elements tags = doc.select("ul.post-tags");    	
    	//System.out.println("<Tags #>: " + tags.size());
    	
    	// Store the set of tags of each article
    	ArrayList<String> myList;
    	//ArticleTag at;
    	ArrayList<ArticleTag> alTag = new ArrayList<ArticleTag>();
    	
    	// Assign article URL to tag sets for a category page:    	
    	Elements tagElmts = null;
    	for(int i=0; i<e.size(); i++) {
    		String articleURL = e.get(i).attr("href");
    		//System.out.println("Article URL: " + articleURL);
    		
    		// Get an Element to hold objects from the <tags> of Elements
    		Element el = tags.get(i);
    		tagElmts = el.getElementsByTag("li");
    		//System.out.println("Tag list size: " + tagElmts.size());
    		myList = new ArrayList<String>();
    		for(Element myEl : tagElmts){
    			//System.out.println(myEl.text());   
    			myList.add(myEl.text());    			
    		}
    		
    		//System.out.println("--------------------");
    		
    		// Create an ArticleTag object, and store article URL and tag list    		
    		alTag.add(new ArticleTag(articleURL, myList));
    	}
    	
    	return alTag;
    }
    
    public void pause(int time) {
    	System.out.println("Pausing the process...");
		try {
			Thread.sleep(time);
		} catch (Exception e){
			System.err.println(e.toString());
		}
    }
    
    // Method for parsing String-valued date and time
 	static Date parseDate(String value) throws ParseException {
 		String tempDate = value.replace(" ET ", " ");
		DateFormat df = new SimpleDateFormat("h:mm a MMM d, y");
		Date date = df.parse(tempDate);
		// System.out.println(date);
	 	return date; 
 	}	
    
 	// Write a method that will cycle through all pages of a business topic (for example: Accelerator)
 	
    public static void main(String[] args) throws IOException, ParseException {
    	System.out.println("<The app. has started>\n");   
    	
    	ListResources lr = new ListResources();
    	
    	ArrayList<ArticleTag> at = new ArrayList<ArticleTag>();
    	
    	// Category URL
    	String categoryURL = "http://blogs.wsj.com/accelerators/page/1/";
    	
    	// Article URL
    	String url = "http://blogs.wsj.com/accelerators/2015/07/02/ed-zimmerman-the-growing-scarcity-of-series-b-venture-rounds/";
		
		//lr.articleElements(url);
    	at = lr.getTags(categoryURL);
		
		System.out.println("\n<The app. has ended>");
    	
/*    	
    	// A repetition cycle will loop over all the pages of a business topic, starting from the first one,
    	// until an exception is reached.
    	
    	for(int i=1; i<=1; i++) {
    		lr.listArticles("http://blogs.wsj.com/accelerators/", i);
    		lr.pause(3000);
    	}
    	
    	System.out.println("\nThe total number of articles fetched: " + lr.numArticles);
    	System.out.println("\nThe app. has successfully ended.");
*/
    }
}