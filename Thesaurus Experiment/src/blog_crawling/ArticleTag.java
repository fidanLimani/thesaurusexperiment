package blog_crawling;

import java.util.ArrayList;

/*
  This class represents article URL and its corresponding tags
 */

public class ArticleTag {
	private String url;
	private ArrayList<String> tags;
	
	public ArticleTag(String url, ArrayList<String> tags){
		this.url = url;
		this.tags = tags;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public ArrayList<String> getTags() {
		return tags;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}
	
	public String toString() {
		String result = "";
		result += "Article URL: " + url + 
					"\nArticle Tags: ";
		
		for(int i=0; i<tags.size(); i++){
			result += tags.get(i) + "; ";
		}
		
		return result;		
	}
	
	// Test the app.
	public static void main(String[] args){
		String articleURL = "http://blogs.wsj.com/accelerators/2015/07/02/ed-zimmerman-the-growing-scarcity-of-series-b-venture-rounds/";
		ArrayList<String> al = new ArrayList<String>();
		al.add("Culture");
		al.add("Investors/Raising Capital");
		al.add("Ed Zimmerman");
		al.add("investor");
		
		ArticleTag at = new ArticleTag(articleURL, al);
		System.out.println("ArticleTag details: \n" + at.toString());
	}
}