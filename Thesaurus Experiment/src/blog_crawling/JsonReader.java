package blog_crawling;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import org.json.simple.parser.JSONParser;

public class JsonReader 
{
    private String articleUrl = 
            "http://blogs.wsj.com/accelerators/2015/05/15/weekend-read-whats-hindering-european-startups/";
    
     int fbRecommend(String articleUrl) 
     {
        String fbGraph = "https://graph.facebook.com/?ids=";
        
        URL webSite;
        String content = "";
        String line;
        
        // Read the URL content
        try {
            webSite = new URL(fbGraph + articleUrl);
            URLConnection conn = webSite.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            while ((line = in.readLine()) != null) {
                content += line;
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        JSONParser parser = new JSONParser();
        KeyFinder finder = new KeyFinder();        
        finder.setMatchKey("shares");
        
        try {
            while (!finder.isEnd()) {
                parser.parse(content, finder, true);
                if (finder.isFound()) {
                    finder.setFound(true);
                }
            }
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
       // What if there is no FB shares value, and it represents a null value?
        if(finder.getValue() == null){
        	return 0;
        } else{ 
        	return Integer.parseInt(finder.getValue().toString());
        }
    }
    
    // Demo the app
    public static void main(String[] args){
        JsonReader json = new JsonReader();
        System.out.println(json.fbRecommend(json.articleUrl));
    }
}