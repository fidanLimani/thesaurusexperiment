package blog_crawling;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import maui.main.MauiWrapper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import db_ops.CRUDOps;
import text_analytics.OpenNLP;
import utils.FileGeneration;

/*
 * We are fetching blog articles from the WSJ Business Blogs, matching the following CSS rules: 
 * .wsjblog and .headlineSummary
 * */

public class ArticleResources 
{		
	/* Temp object to store the attributes of ArticleTag obejcts from the
	 * resulting getTag() method invocation.
	*/
	private Article tempArticle;
	
	// This is the first list that gets populated once the blog crawling begins.
	// This list gets populated by 10 article URLs and related tags, per category page.
	private ArrayList<ArticleTag> urlAndTags;
	
	/* This field stores all the details of an article - it completes the information 
	 * that has already been provided in terms of URL and article tags.
	 * This field is going to be passed to another object, which will persist it to a
	 * Relational and Graph DB.
	 * */ 
	private ArrayList<Article> fullArticles;
	
	// DB-related field
	CRUDOps db;
	
	/* 
	 * FilesGenerator field to generate a "training" version of each blog post
	 * to be used during MAUI model training.
	*/
	private FileGeneration fg;
	
	// MAUI indexing tool
	private MauiWrapper maui;	
    
    // The WSJ Blogs Categories by Business Topic
    String[] res = {"http://blogs.wsj.com/accelerators/",
        "http://blogs.wsj.com/bankruptcy/",
        "http://blogs.wsj.com/developments/",
        "http://blogs.wsj.com/digits/",
        "http://blogs.wsj.com/law/",
        "http://blogs.wsj.com/numbersguy/",
        "http://blogs.wsj.com/privateequity/",        
        // "http://blogs.wsj.com/tech-europe/", --> This category has merged with "Digits";
        "http://blogs.wsj.com/venturecapital/"};
    
    // Class constructor
    public ArticleResources() {    
    	tempArticle = null;
    	urlAndTags = new ArrayList<ArticleTag>();
    	fullArticles = new ArrayList<Article>();
    	fg = new FileGeneration();
    	db = new CRUDOps();
    	maui = new MauiWrapper("../Maui1.2/", "stw", "keyphrextr");
    }
    
    /* This method lists all the articles of a category
     * An example URL for a category: http://blogs.wsj.com/accelerators/page/4/
     * The first page is http://blogs.wsj.com/accelerators/page/1/.
     * */     
    public void listArticles(String url, int pageNumber, int fileNumber) throws IOException, ParseException 
    {     	
    	url += "page/" + pageNumber + "/";
    	
    	// Retrieve <URL and Tag> sets of articles on a given category page
    	urlAndTags = getTags(url);
    	
    	// System.out.println("After the fetched category page, there are <" + urlAndTags.size() + "> objects stored.");
    	// System.out.println("Processing <" + url + "\n");
    	
    	// Invoke the Article methods on each objects stored in the <urlAndTags> array list.
    	fullArticles = getArticles(urlAndTags);
    	    	
    	// Loop over the <fullArticles> list: Invoke the DB-related method to store 
    	// <fullArticles> array in a relational and/or graph DB;
    	
    	/* TEST: print out the Article objects stored in the fullArticle array list
    	for(Article a : fullArticles){
    		System.out.println(a.toString());
    	}
    	*/
    	
    	// Set the current file number to be created in this batch; this is for naming conventions...
    	
    	fg.count = fileNumber;
    	for(int i=0; i<fullArticles.size(); i++){
    		System.out.println("DB operations: " + (i+1));
    		db.performCreate(fullArticles.get(i));
    		
    		System.out.println("File generation: " + (fg.count));    		
    		fg.run((fg.count), "Bankruptcy", fullArticles.get(i));
    	}
    	
    }
    
    /* A method that takes an ArticleTag object as an input (from where it uses article URL and tag set), 
     * and completes an Article object with all the required metadata (title, publication date, FB shares, etc.)
     */
    public ArrayList<Article> getArticles(ArrayList<ArticleTag> at) throws IOException, ParseException {
    	/* tempArticle - to temporarily store and complete an article;
    	   fullArticle - a list of completed articles 	
    	   tempArticleTags - only used to remove the author from a list of tags
    	*/
    	
    	ArrayList<Article> tempList = new ArrayList<Article>();
    	ArrayList<String> tempArticleTags = new ArrayList<String>();
    	// Loop over the ArticleTag array list
    	for(ArticleTag articleT : at){    		
    		// 0. Create an Article object using the URL in the ArticleTag object
    		tempArticle = new Article(articleT.getUrl());    		    		
    		System.out.println("\nProcessing article URL: " + articleT.getUrl());    		    		
    		
    		// 1. Complete the Article data: Add the tags from the ArticleTag object
    		tempArticle.setTags(articleT.getTags());
    		
    		// 2. Invoke the rest of the Article methods (encompassed in run() method)
    		tempArticle.setProperties();
    		
    		// 3. Remove the author from the tag list.
    		tempArticleTags = tempArticle.getTags();
    		tempArticleTags.remove(tempArticle.getAuthor().toString());
    		
    		/* For testing purposes;
    		for(String str : tempArticleTags){
    			System.out.println("Tag: " + str);
    		} */
    		
    		for(int i=0; i<tempArticleTags.size(); i++){
    			String tempAuthor = tempArticleTags.get(i);
    			// System.out.println("Processing tag: " + tempAuthor); // For testing purposes;
    			if(tempAuthor.equalsIgnoreCase(tempArticle.getAuthor())){
    				tempArticleTags.remove(i);
    				// System.out.println(tempAuthor + " -> removed from tags!");
    			}
    		}    		    		    		
    		
    		// 4. Add keywords extracted from the Article object content (MAUI is not trained
    		// at this point; STW is used as a controlled vocabulary.    		
    		// Combine all blog post metdata (title, content, publication date)
    		String combinedMetadata = tempArticle.getTitle() +
    				tempArticle.getContent() + tempArticle.getPubDate();
    		try {
				tempArticle.setKeywords(maui.extractTopicsFromText(combinedMetadata, 5));
			} catch (Exception e) {
				e.printStackTrace();
			}    		
    		
    		// Store the complete Article in an array list, to be returned at the end of this method call
    		tempList.add(tempArticle);
    	}
    	
    	return tempList;
    }
    
    /* A method to get the tags/article from e blog category page:
     * E.g.: Get article/tags from page 2 from "Accelerators" category. 
     */
    public ArrayList<ArticleTag> getTags(String categoryURL) throws IOException 
    {
    	// Example: http://blogs.wsj.com/accelerators/page/1/ could be passed in
    	Document doc = Jsoup.connect(categoryURL).timeout(20*1000).get();

    	// All blog titles for this category page (Example, all articles on page 3)
    	// Note: Some article have an image with the <a> element, thus contributing to more 
    	// than 10 results, thus the use of ":not" pseudo selector to filter this info out.
    	Elements e = doc.select(".post-snippet .post-title").select("a:not(.post-thumb)");     	
    	// System.out.println("TEST... # of articles: " + e.size());
    	
    	// All blog article tags for this category page (Example, all article tags on page 3)
    	Elements tags = doc.select("ul.post-tags");    	
    	// System.out.println("TEST... <Tags #>: " + tags.size());
    	
    	// Store the set of tags of each article
    	ArrayList<String> myList;    	
    	ArrayList<ArticleTag> alTag = new ArrayList<ArticleTag>();    	
    	
    	Elements tagElmts = null;
    	for(int i=0; i<e.size(); i++) {
    		String articleURL = e.get(i).attr("href");
    		// System.out.println("TEST... Article URL: " + articleURL);    		
    		// Get an Element to hold objects from the <tags> of Elements
    		Element el = tags.get(i);
    		tagElmts = el.getElementsByTag("li");
    		//System.out.println("Tag list size: " + tagElmts.size());
    		myList = new ArrayList<String>();
    		for(Element myEl : tagElmts){
    			//System.out.println(myEl.text());
    			
    			// Condition: if the tag represents the author name, don't include it!
    			myList.add(myEl.text());    			
    		}
    		
    		//System.out.println("--------------------");
    		
    		// Create an ArticleTag object, and store article URL and tag list    		
    		alTag.add(new ArticleTag(articleURL, myList));
    	}    	
    	return alTag;
    }

    /*
     * A utilities app. that prints out the content of an array list of Article objects
     * */
    public void displayArticles(ArrayList<Article> articles){
    	System.out.println("\n\nDisplaying the content of the articles ... ");
    	for(Article a : articles){
    		System.out.println(a.toString());
    		System.out.println("----------------------------------------\n");
    	}
    }
    
    // Getter/Setter for fg field
    public int getFg(){
    	return fg.count;
    }
    
    public void setFg(int n){
    	this.fg.count = n;
    }
    
    /* 								
     * Test the app.
     * 
     * */
    public static void main(String[] args) throws ParseException {
    	System.out.println("<The app has started...>\n");
    	
    	ArticleResources ar = new ArticleResources();
    	String url = "http://blogs.wsj.com/accelerators/"; 
    	int pageNumber = 51;        	
    	int fileNumber = 1;
    	
    	try	{
    		ar.listArticles(url, pageNumber, fileNumber);
    	} catch (IOException e){
    		e.printStackTrace();
    	}
    	
    	/*
    	
    	
    	while(true)
    	{
    		try {
    			System.out.println("Processing page: " + pageNumber);
    			System.out.println("-------------------");
				ar.listArticles(url, pageNumber, fileNumber);
				pageNumber++;
				break;
			} catch (IOException e) {
				System.out.println("Out of web pages!");				
				e.printStackTrace();
			}
    	}    	
    	*/
    	
    	System.out.println("\n<The app has completed.>");
    }
}