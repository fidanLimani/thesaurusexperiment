package blog_crawling;

import java.io.IOException;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;

/*
 *	This class demonstrates the usage of the ArticleResource class: crawling, retrieving and
 *	storing social web resources metadata in a relational DB, as well as generating training
 *	files required by the automatic indexing tool MAUI.  
 * */

public class ResourcesDemo 
{
	public static void main(String[] args) throws ParseException {
		System.out.println("<The app has started...>\n");
    	
    	ArticleResources ar = new ArticleResources();
    	// Start with a Wall Street Journal category:
    	String url = "http://blogs.wsj.com/bankruptcy/";   	
    	int pageNumber = 25;    
    	int fileNumber = 2015;    	
    	
    	System.out.println("Processing page: " + pageNumber);
		System.out.println("-------------------");
		try {
			ar.listArticles(url, pageNumber, fileNumber);
		//	System.out.println(ar.getFg().);
		} catch (IOException e) {
			e.printStackTrace();
		}	
		    	
    	/* while (true) cycle planned to run until there are no more pages, but
    	   due to network processing, I split the run in several batches of 50 
    	   pages each
    	
    	while(pageNumber < 10)
    	{
    		try {
    			System.out.println("Processing page: " + pageNumber);
    			System.out.println("-------------------");
				ar.listArticles(url, pageNumber, fileNumber);				
				pageNumber++;
				ar.setFg(ar.getFg() + 1);
				
				// Insert a break of 15 seconds after processing each category page
				try {
					System.out.println("Sleeping for 15 seconds... \n");
					TimeUnit.SECONDS.sleep(15);
				} catch (InterruptedException e){
					e.printStackTrace();
				}
			} catch (IOException e) {
				System.out.println("Out of web pages!");				
				e.printStackTrace();
			}
    	} 	
    	*/
    	System.out.println("\n<The app has completed.>");
	}
}