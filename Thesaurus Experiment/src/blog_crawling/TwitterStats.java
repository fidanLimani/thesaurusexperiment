package blog_crawling;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.json.simple.parser.JSONParser;

// Retrieve the tweet number of of a URL

public class TwitterStats 
{	
	int twitterCount(String articleUrl) throws IOException 
    {
       String twitterLink = "http://cdn.api.twitter.com/1/urls/count.json?url=";
       
       URL webSite;
       String content = "";
       String line;
       
       try {
    	   webSite = new URL(twitterLink + articleUrl);
    	   URLConnection conn = webSite.openConnection();
           BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

           while ((line = in.readLine()) != null) {
               content += line;
           }
       } catch (MalformedURLException e) {	
		e.printStackTrace();
       }
       
       JSONParser parser = new JSONParser();
       KeyFinder finder = new KeyFinder();        
       finder.setMatchKey("count");

       try {
           while (!finder.isEnd()) {
               parser.parse(content, finder, true);
               if (finder.isFound()) {
                   finder.setFound(true);
               }
           }
       } catch (Exception e) {
       	e.printStackTrace();
       }
       
      return Integer.parseInt(finder.getValue().toString());
    }
	
	public static void main(String[] args) throws IOException{
		String url = "http://blogs.wsj.com/accelerators/2015/07/02/ed-zimmerman-the-growing-scarcity-of-series-b-venture-rounds/";
		TwitterStats ts = new TwitterStats();
		System.out.println(ts.twitterCount(url));
	}
}