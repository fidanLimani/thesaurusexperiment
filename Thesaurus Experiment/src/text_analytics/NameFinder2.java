package text_analytics;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;

/*
 * Source: Natural Language Processing with Java by Richard M Reese; Ch. 4;
 * */

public class NameFinder2 {
	
	public static void find(String sentence)
	{
		System.out.print("The sentence to be analyzed:\n");
		System.out.println("<" + sentence + ">\n");
		
		// Use the models en-token.bin and en-ner-person.bin files for the
		// tokenizer and name finder models, respectively.
		try (InputStream tokenStream = new FileInputStream(new File(getModelDir(), "en-token.bin"));
                InputStream modelStream = new FileInputStream(new File(getModelDir(), "en-ner-person.bin"));)
        {
			TokenizerModel tokenModel = new TokenizerModel(tokenStream);
			TokenizerME tokenizer = new TokenizerME(tokenModel);
			
			TokenNameFinderModel entityModel = new TokenNameFinderModel(modelStream);
			NameFinderME nameFinder = new NameFinderME(entityModel);
			
			// Single sentence
            {                                
                String[] tokens = tokenizer.tokenize(sentence);
                Span[] nameSpans = nameFinder.find(tokens);
                
                // Calculate precision
                double[] spanProbs = nameFinder.probs(nameSpans);

                for (int i = 0; i < nameSpans.length; i++) {
                    System.out.println("Span: " + nameSpans[i].toString());
                    System.out.println("Entity: " + tokens[nameSpans[i].getStart()]);
                    System.out.println("Probability: " + spanProbs[i]);
                }
            }

		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public static File getModelDir() {
        return new File("C://Users//fidan//Documents//NetBeansProjects//Lib//apache-opennlp-1.5.3//Name finder model//");
    }
	
	// Test the app.						//
	public static void main(String[] args)
	{
		System.out.println("<Application started>\n");
		
		// Pass this variable to the method find(String ...)
		String sentence = "The 86-year-old Reagan will remain overnight for observation at " + 
				"a hospital in Santa Monica, California, said Joanne " +
				"Drake, chief of staff for the Reagan Foundation.";
		NameFinder2.find(sentence);
		
		System.out.println("\n<Application completed.>");
	}
}