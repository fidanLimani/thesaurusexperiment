package text_analytics;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

/*
 * 
 * */

public class Test 
{
	public static void main(String[] args){
		System.out.println("<Application started>\n");
		
		/*
		File f = new File("C://Users/fidan/Documents/NetBeansProjects/Lib/jate_1.11/nlp_resources");
		ArrayList<File> files = new ArrayList<File>(Arrays.asList(f.listFiles()));
		
		// Find the following file
		// nlp_resources\bnc_unifrqs.normal
		
		for(File file : files){
			System.out.println(file.getName());
		} */
		
		// Testing a regular expression for removing the [ and ] characters from a string object.
		System.out.println("[John Doe]".replaceAll("[\\[\\]]", ""));
		
		System.out.println("\n<Application completed.>");
	}
}
