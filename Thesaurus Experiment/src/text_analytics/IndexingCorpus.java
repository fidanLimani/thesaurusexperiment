package text_analytics;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LimitTokenCountAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

/* Index a corpus of data in order to balance the term weight in a given text. In this case, the 
 * domain of interest is business and economics.
 * 
 * Based on the works in: https://today.java.net/article/2003/07/28/lucene-intro
 * */

public class IndexingCorpus
{
	private int txtFileCount;
	
	public IndexingCorpus(){
		txtFileCount = 0;
	}
	
	// Test whether we are treating a directory
	public void index(File indexDir, File dataDir) throws IOException 
	{
        if (!dataDir.exists() || !dataDir.isDirectory()) {
            throw new IOException(dataDir + " does not exist or is not a directory");
        }
    
        Directory fsDir = FSDirectory.open(indexDir);
        
        Analyzer stdAn = new StandardAnalyzer(Version.LUCENE_31);
        Analyzer ltcAn = new LimitTokenCountAnalyzer(stdAn,Integer.MAX_VALUE);
        
        IndexWriterConfig iwConf = new IndexWriterConfig(Version.LUCENE_31, ltcAn);
        iwConf.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        
        IndexWriter indexWriter = new IndexWriter(fsDir, iwConf);        
        indexDirectory(indexWriter, dataDir);
        indexWriter.close();
    }
	
	// List all the files of a given data corpus
	private void indexDirectory(IndexWriter writer, File dir) throws IOException {
	    File[] files = dir.listFiles();
	    
	    for (int i=0; i < files.length; i++) {
	        File f = files[i];	       
	        if (f.isDirectory()) {
	           indexDirectory(writer, f);  // recurse
	        } else if (f.getName().endsWith(".txt")) {
	           indexFile(writer, f);
	           this.txtFileCount++;
	        }
	    }
	}
	
	// Index a .txt file 
	private void indexFile(IndexWriter writer, File f) throws IOException {
	    System.out.println("Indexing " + f.getName());

	    Document doc = new Document();
	    doc.add(new Field("contents", new FileReader(f)));	    
	    doc.add(new Field("filename", f.getCanonicalPath(), Store.YES, Index.ANALYZED));
	    writer.addDocument(doc);
	}
	
	// Test the app
	public static void main(String[] args) throws IOException {		
		IndexingCorpus it = new IndexingCorpus();
		
		File docDir = new File("C://Users//fidan//Downloads//OANC_GrAF//data");
		File indexDir = new File("indexDir");
		
		it.index(indexDir, docDir);
		System.out.println("Total # of txt files: " + it.txtFileCount);
	}
}