package text_analytics;

/*
 * Multi Entity Finder: Combining many OpenNLP NER models.
 *  
 * Source: Natural Language Processing with Java by Richard M Reese; Ch. 4; * 
 * */

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;

import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;

public class MEntityFinder 
{
	private static void usingMultipleNERModels(String sentence) {
        // Models - en-ner-person.bin en-ner-location.bin en-ner-money.bin 
        // en-ner-organization.bin en-ner-time.bin
        try {
            InputStream tokenStream = new FileInputStream(
                    new File(getModelDir(), "en-token.bin"));

            TokenizerModel tokenModel = new TokenizerModel(tokenStream);
            TokenizerME tokenizer = new TokenizerME(tokenModel);

            String modelNames[] = {"en-ner-person.bin", "en-ner-location.bin", "en-ner-organization.bin"};            
            ArrayList<String> list = new ArrayList<String>();
            
            for (String name : modelNames)
            {
                TokenNameFinderModel entityModel = new TokenNameFinderModel(
                        new FileInputStream(new File(getModelDir(), name)));
                NameFinderME nameFinder = new NameFinderME(entityModel);
                
                // Look for different model entities, such as: Location; Organization; or Person
                String[] tokens = tokenizer.tokenize(sentence);
                Span[] nameSpans = nameFinder.find(tokens);
                
                // Calculate precision
                double[] spanProbs = nameFinder.probs(nameSpans);
                
                System.out.println("Using the <" + name + "> model: \n");
                for (int i = 0; i < nameSpans.length; i++) {
                    System.out.println("Span: " + nameSpans[i].toString());
                    System.out.println("Entity: " + tokens[nameSpans[i].getStart()]);
                    System.out.println("Probability: " + spanProbs[i]);
                }
                
            }
            
            System.out.println("Multiple Entities");
            for (String element : list) {
                System.out.println(element);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
	
	// A little util. method to determine the model path.
	public static File getModelDir() {
        return new File("C://Users//fidan//Documents//NetBeansProjects//Lib//apache-opennlp-1.5.3//Name finder model//");
    }
	
	// Test the app.
	public static void main(String[] args){
		String sentence = "ED ZIMMERMAN: The funnel for venture funding isn�t cylindrical � it�s shape follows a more Darwinian conical path, as many "
							+ "seed stage companies march into the cone�s wide entrance and far fewer make it to the cone�s narrow end. There has been "
							+ "a lot of discussion recently of where the choke points are in the cone, and I believe we�re headed into a period in which "
							+ "the Series B venture round will be more of a choke point than it has been in the past, especially for American startups.\n"
							+ "I�ve been obsessing about choke points in the venture funnel since preparing for a talk I recently gave in Tel Aviv. Studies "
							+ "show that in the handful of major nations with vibrant venture markets, more than half the deals are �seed rounds� or �Series A� "
							+ "venture rounds (in plain terms, the first or second round of funding for a startup). For instance, in first quarter 2015, 55% of "
							+ "all American venture rounds were either seed or Series A, split almost evenly, while 19% of all rounds were Series B (the third "
							+ "round of financing), according to data from CB Insights. For the 12 months ended March 31 of this year, the statistics for tech "
							+ "startups show that seed and Series A rounds collectively accounted for 58% of all American venture rounds, 66% of Israeli rounds "
							+ "and a staggering 71% of all European venture rounds. In order to survive, most of those companies will need to raise another round "
							+ "within the 18 months that follow their financings. While you can sell your seed round on a founder�s dream and vision, you have to "
							+ "have a team and generally a launched product or service for your Series A round. Your Series B round, however, requires traction or, "
							+ "in fewer situations, some new telling of the tale (a superstar has joined the team, you�ve uncovered a new application for the technology, "
							+ "the startup has inked a massive strategic deal, etc.). Metrics matter most in Series B, or as the venture community likes to say: �There�s "
							+ "nothing like numbers to screw up a good story,� and render a promising startup unfinanceable.\n"
							+ "One of the best graphic depictions of the cone of venture funding was done by CB Insights, which tracked a cohort of the 160 American tech "
							+ "startups that closed on seed funding in 2009. The graphic follows those startups until April 2014. The research found that only 54% of those "
							+ "startups landed Series A funding, while 36% of the original cohort ended up nabbing elusive Series B money by April 2014. That number got roughly "
							+ "halved, as a mere 20% of the original 160 startups made it to Series C funding. Only 4% of the 160 startups from the class of 2009 completed a 6th "
							+ "funding round by April 2014."
							+ "Does that mean that a third of seeded tech startups will complete a Series B round? Certainly not. First of all, the CB Insights data started with "
							+ "companies where the seed round included at least one institutional investor. Other data has established that when the round doesn�t include an actual "
							+ "seed or venture fund, the likelihood of subsequent funding is lower. Moreover, seed funding was scarcer in 2009, so startups that obtained seed funding "
							+ "at that time had passed a tougher screen to do so. Since then, the global allocation to seed funding has significantly increased. For instance, as I�ve "
							+ "previously written, �In 2011, only 28% of Europe�s venture-backed tech deals were seed stage� [but] in 2013 and 2014, roughly half of all European tech "
							+ "venture deals were seed stage.� So more of these companies march into the wide mouth of the funnel."
							+ "Because the U.S. is the primary provider of venture capital to itself and the rest of the world, the companies vying for these funds are now more global "
							+ "than ever. If you don�t think the competition for Series B funding is very global, you need only consider very stale data from Canada. Between 2000 and 2002, "
							+ "Industry Canada reported that roughly a quarter of the venture funding for Canadian startups came from the United States, while the converse was not true � "
							+ "Canadian venture capitalists maybe accounted for 1% of venture investments into U.S. companies. (See page 287 of Josh Lerner and Antoinette Schoar�s International "
							+ "Differences in Entrepreneurship). More recently, in commenting on the much better-developed Israeli venture markets, a report concluded: �the returns from the "
							+ "appreciation in value of VC-backed Israeli companies are mostly enjoyed by foreign investors.� This trend of foreign startups seeking earlier and earlier U.S. "
							+ "venture and seed funding will intensify in the coming years. The trend is further reinforced as American venture capitalists continue to chase yield by investing "
							+ "in less value-inflated markets, which includes non-U.S. markets, especially where a larger check size justifies the transaction costs inherent in doing an international "
							+ "deal." 
							+ "And speaking of value, the valuations in 2009 � when CB Insights began tracking that cohort � were lower, making it easier to fund in the next round. Remember the �buy low, "
							+ "sell high� advice they were supposed to teach you in business school? It is harder to fund an over-valued venture-backed startup in a Series B round than an under-valued one, "
							+ "especially because founders and incumbent investors shy away from down-rounds, particularly in a robust market where they�re not seeing others take lower valued rounds."
							+ "Moreover, a graph of the money flowing into American funds would look like a barbell � seed and early stage funds continue to grow in headcount, while there are fewer new "
							+ "places to find funding in the middle until you get to the very late stage, where there�s plenty of capital for highly successful companies. According to the National Venture "
							+ "Capital Association data for 2015�s first quarter, venture funds raised fewer deployable dollars (which we�ll start to feel in a few years) than the highs reportedly reached "
							+ "in 2014. Of the 63 funds raised in the first quarter, 27 are early stage, 8 are seed funds and only 16 are devoted to Series B. Even among the Series B funds, however, some "
							+ "are very small and only 5 (or less than 10% of all funds raised that quarter) are $200 million or greater. That�s not much firepower to support what�s coming through an "
							+ "increasingly conical funnel. It is worth noting that this column relies on third part data to fit both my personal observations of the market and, consistent with those, "
							+ "a discussion in San Francisco at last week�s VentureCrush in which I participated with leading venture investors as co-hosts: Aileen Lee (Cowboy Ventures), Chris Moore "
							+ "(Redpoint Ventures), Hunter Walk (HomeBrew), Beezer Clarkson (Saphire Ventures) and Jules Maltz (IVP). There were numerous others in the room and in writing this, I�m not "
							+ "reporting on the views of any of the foregoing investors � they facilitated a robust conversation rather than dictated conclusions. (full disclosure: I�m personally an "
							+ "investor in funds managed by HomeBrew and Cowboy and founded VentureCrush.)";
		
		MEntityFinder.usingMultipleNERModels(sentence);
	}
}
