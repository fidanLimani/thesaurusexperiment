package text_analytics;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class OpenCalaisKE 
{
	String licenseID = "dxmdj88nhh5x5rbcpafhsvrr";
	
	// 1. Web service URL for improved REST API is located at http://api.opencalais.com/tag/rs/enrich
	private static final String CALAIS_URL = "http://api.opencalais.com/tag/rs/enrich";
	
	HttpClient httpClient;
	// 2. Clients should create an HTTP POST request.
	HttpPost postMethod;	
	
	public OpenCalaisKE(){
		httpClient = HttpClients.createDefault();		 
	}
	
	private HttpPost postMethod(String content) 
	{
        postMethod = new HttpPost(CALAIS_URL);

        // Set mandatory parameters
        postMethod.addHeader("x-calais-licenseID", licenseID);
        postMethod.addHeader("Content-type", "text/raw; charset=UTF-8");
        
        // 3. Document content should be passed as the body of the HTTP request.
        StringEntity se = new StringEntity(content, ContentType.create("text/plain", "UTF-8"));
        postMethod.setEntity(se);
        
		// Set response/output format
        //postMethod.addHeader("Accept", "xml/rdf");
        postMethod.addHeader("Accept", "application/json");

        // Enable Social Tags processing
        postMethod.addHeader("enableMetadataType", "SocialTags");

        return postMethod;
    }
	
	private void doRequest(HttpPost method) throws ClientProtocolException, IOException {
		HttpResponse httpResponse = httpClient.execute(method);
		HttpEntity entity = httpResponse.getEntity();
		
		if (entity != null) {		    
		   	try {
				System.out.println("Entity results: \n" + EntityUtils.toString(entity));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				System.err.println(e);
			}
		}	
	}
	
	public static void main(String[] args) throws ClientProtocolException, IOException {
		OpenCalaisKE op = new OpenCalaisKE();
		String content = "Noam Wasserman, the Harvard Business School professor and author of The Founder�s Dilemmas, found an interesting " +
						"trend in the tenure of startup founder-CEOs: in his analysis of more than 200 startups, fewer than 50% of CEOs remained " + 
						"after three years. This number dropped to 40% after four years and 25% through the IPO. It�s a notable but unsurprising " + 
						"statistic � nearly all founders reach an inflection point where their responsibilities broaden and new challenges emerge." +
						" Since my co-founders and I started Invoca in 2008, my role as founder-CEO has broadened from proving a concept and building " + 
						"a team to managing personnel and scaling operations. With a few exceptions � the Gates, Zuckerbergs and Ellisons of the " +
						"world � these are very different skill sets. And all of your stakeholders benefit when you understand how your skills and passions " +
						"align with each stage of your company�s growth. I�m an early stage guy. There. I said it. I like to create and prove things. I like " +
						"to do things that people think can�t be done. I like to light the light bulb for people. In the past year or so, Invoca has entered a " +
						"new stage that requires a different skill set. In the midst of our success, I found myself increasingly doing more of the stuff I " +
						"don�t enjoy and less of the stuff I do. So, with my founder hat off and my shareholder hat on, I set out to find a new CEO with a " +
						"skill set and experience that�s a better fit for Invoca�s next stage of growth. The bar was very high. We�re incredibly fortunate to " +
						"have found an experienced public SaaS company CEO who�s joining Invoca as we prepare for an expected IPO. Here�s what I�ve learned so " +
						"far from the process of hiring a new CEO:";
		
		HttpPost myMethod = op.postMethod(content);
		op.doRequest(myMethod);
	}

}
