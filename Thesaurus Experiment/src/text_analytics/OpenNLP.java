package text_analytics;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;
/*
 * Source: Ch. 5 from "Taming Text", Manning, 2013;
 * 
 * This class is to be used for identifying blog authors!
 * */

public class OpenNLP
{		
	// Data fields for storing: Persons, Organizations, Money, Locations, etc., using OpenNLP
	public ArrayList<String> persons = new ArrayList<String>();
		
	public ArrayList<String> find(String[] sentences) {		
		try (InputStream tokenStream = new FileInputStream(new File(getModelDir(), "en-token.bin"));
                InputStream modelStream = new FileInputStream(new File(getModelDir(), "en-ner-person.bin"));)
        {				
			TokenizerModel tokenModel = new TokenizerModel(tokenStream);
			TokenizerME tokenizer = new TokenizerME(tokenModel);
			
			NameFinderME finder = new NameFinderME(new TokenNameFinderModel(modelStream));				
        
			for (int si = 0; si < sentences.length; si++) {
				String[] tokens = tokenizer.tokenize(sentences[si]);
				Span[] names = finder.find(tokens);
				double[] spanProbs = finder.probs(names);
				displayNames(names, tokens);			
				
				// Display Names and probabilities
				// displayNamesProbs(names, tokens, spanProbs);
			}
			finder.clearAdaptiveData();			
        } catch (IOException e){
        	e.printStackTrace();;
        }		
		
		return persons;
	}
	
	private void displayNames(Span[] names, String[] tokens) {
		StringBuilder cb = null;
		for (int si = 0; si < names.length; si++) {
			cb = new StringBuilder();
			for (int ti = names[si].getStart(); ti < names[si].getEnd(); ti++) {
				cb.append(tokens[ti]).append(" ");
			}
			// System.out.println(cb.substring(0, cb.length() - 1));
			persons.add(cb.substring(0, cb.length() - 1).toString());
			// System.out.println("ttype: " + names[si].getType());			
		}
	}
	
	/* If we want to filter based on OpenNLO probabilities that a name has been found, we
	 * could use the following method. However, since name recognition is with pretty high
	 * probabilities (in the (80 - 90) % range), we do not apply it at this time.
	
	private void displayNamesProbs(Span[] names, String[] tokens, double[] probs) {
		StringBuilder cb = null;
		for (int si = 0; si < names.length; si++) {
			cb = new StringBuilder();
			for (int ti = names[si].getStart(); ti < names[si].getEnd(); ti++) {
				cb.append(tokens[ti]).append(" ");
			}
			cb.append(probs[si]);
			System.out.println(cb.toString());			
			// System.out.println("ttype: " + names[si].getType());			
		}
	}
	*/
	
	public static File getModelDir() {
        return new File("C://Users//fidan//Documents//NetBeansProjects//Lib//apache-opennlp-1.5.3//Name finder model//");
    }
	
	public static void main(String[] args) throws IOException {
		System.out.println("<The app has started>\n");
		System.out.println("Excluding person names from a list of value (tags, in this case): \n");
		
		// This array is just for testing purposes, as the main class relies on ArrayList to store tags;
		ArrayList<String> temp = new ArrayList<String>();		
		
		String[] sentences = { "CULTURE", "Investors/Raising capital", "Ed Zimmerman", "Investor", "John Doe" };	
		
		OpenNLP nlp = new OpenNLP();
		
		nlp.find(sentences);
		
		System.out.println("<" + nlp.persons.size() + "> person(s) found!");		
		for(String str : nlp.persons){
			System.out.println(str);
		}
		
		System.out.println("\nRemoving the author from the tags list: ");
		for(String s : sentences){
			if(s.equalsIgnoreCase(nlp.persons.get(0))){
				continue;
			} else {
				System.out.print(s + "\t");
			}
		}
		
		System.out.println("\n<The app has completed>");
	}
}
