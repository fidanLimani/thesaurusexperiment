package utils;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import blog_crawling.Article;

/*
 * This class is used to create training files for usage with the MAUI
 * indexing tool.
 * 
 * The output from this class' operations is:  * 
 * 	1. <.key> file, including the original blog post tags;
 * 	2. <.text> file, including the blog post title, content, author, and date published;
 * 
 *  The title of the file is the same, whereas the extension is <.key> and <.txt>, respectively.
 * */

public class FileGeneration 
{
	private FileWriter fw;
	private BufferedWriter bw;
	
	// An integer counter used to hold the value of the
	public int count;
	
	public FileGeneration(){
		// this.tags = new ArrayList<String>();
		// this.textContent = "";
		fw = null;
		bw = null;
		count = 0;
	}
	
	public FileGeneration(int i){
		fw = null;
		bw = null;
		this.count = i; // If we run the app. after an eventual brake, we need to be able to continue at the next file number;
	}
	
	// This method creates the <.key> file
	/**
	 * @param str: Blog Post Tags
	 * */
	public void createKey(File file, ArrayList<String> str){
		File f = file;
		try {
			fw = new FileWriter(f.getAbsolutePath());
			bw = new BufferedWriter(fw);
			
			for(String temp : str){
				bw.write(temp + "\n");
			}
			bw.close();			
		} catch (IOException e) {			
			e.printStackTrace();
		}
	}
	
	/*
	 * @param str: Title + Content + Author + Date published
	 * */
	// This method creates the <.txt> file
	public void createText(File file, String str){
		File f = file;
		try {
			fw = new FileWriter(f.getAbsolutePath());
			bw = new BufferedWriter(fw);
			bw.write(str);
			bw.close();			
		} catch (IOException e) {			
			e.printStackTrace();
		}
	}
	
	// Create the two files for the training, and invoke the methods to fill them out
	/*
	 * @param: fileNumber - the number to be used for the files generated;
	 * @param: category - the blog post category (to be used when sampling files for machine learning)
	 * @param: content - blog post title + author + content + publication date, concatenated;
	 * @param: tags - Original blog post tags
	 * */
	public void run(int id, String category, Article a){
		try {   		 
			File fileTxt = new File("mauiAllFiles\\" + category + id + ".txt");
		    File fileKey = new File("mauiAllFiles\\" + category + id + ".key");		      
		      
		    if (fileTxt.createNewFile() && fileKey.createNewFile()){
		      // System.out.println("Files created!");
		      createText(fileTxt, a.getTitle() + "\n"		    		  			
		    		  			+ a.getContent() + "\n"
		    		  			+ a.getPubDate());
		      createKey(fileKey, a.getTags());
		      count++;
		    }		      
	    } catch (IOException e) {
		    e.printStackTrace();
		}
	}
}