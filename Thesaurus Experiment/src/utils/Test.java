package utils;

import java.util.concurrent.TimeUnit;

public class Test 
{
	public static void test() {
		System.out.println("Sleeping for 5 seconds...");
	}
	
	public static void main(String[] args)
	{		
		for(int i=0; i<5; i++){
			try {
				test();
				TimeUnit.SECONDS.sleep(5); // sleep for 5 seconds
			} catch (InterruptedException e) {
				
			}
		}
	}
}