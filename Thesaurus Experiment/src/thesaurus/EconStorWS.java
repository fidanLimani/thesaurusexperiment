package thesaurus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class EconStorWS 
{
    public static void main(String[] args) throws ClientProtocolException, IOException {
    	String searchTerm = "Hedge_funds";
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet("http://zbw.eu/beta/econ-ws/suggest?dataset=econ_corp&query=" + searchTerm);
        HttpResponse response = client.execute(request);
        BufferedReader rd = new BufferedReader (new InputStreamReader(response.getEntity().getContent()));
        String line = "";
        while ((line = rd.readLine()) != null) {
          System.out.println(line);
        }        
    }
}

// Code source: http://harryjoy.com/2012/09/08/simple-rest-client-in-java/

// Target Web Services from ZBW:
// ZBW SPARQL Endpoints (beta): http://zbw.eu/beta/sparql/
// Web Services for Economics (beta): http://zbw.eu/beta/econ-ws/about

/*
 * /suggest - Suggestions for Resources (starting with a given string)
 * Example:
	http://zbw.eu/beta/econ-ws/suggest?&query=acc
	http://zbw.eu/beta/econ-ws/suggest?dataset=jel&query=e2
 * */

/* /suggest2 - Suggestions for Resources (optimized for large datasets)
 * Example: http://zbw.eu/beta/econ-ws/suggest2?dataset=econ_pers&query=ke
 * */
