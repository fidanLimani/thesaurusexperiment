Attracting Investor Attention
@WSJSTARTUP:When should a startup seek its first investment? What should founders know before offering equity? Are pitch-offs and demo days effective? Read what our mentors have to say:
Mon Oct 13 11:00:00 CEST 2014